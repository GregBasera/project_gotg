#!/bin/bash

echo "Compiling..."

cd GUI_imple/PlayerOne
javac PlayerOneGUI.java
gcc -o server GOTG_PlayerOne.c
cd ../PlayerTwo
javac PlayerTwoGUI.java
gcc -o client GOTG_PlayerTwo.c
cd ../../PrePost_Game
javac preGame.java
javac postGame.java

echo "All sources compiled"
