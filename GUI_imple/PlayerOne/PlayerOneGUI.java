import java.io.*; // this loads WHOLE package; not an efficient way to do this; I'm lazy.
import java.awt.*; // this loads WHOLE package; not an efficient way to do this; I'm lazy.
import java.awt.event.*; // this loads WHOLE package; not an efficient way to do this; I'm lazy.
import javax.swing.*; // this loads WHOLE package; not an efficient way to do this; I'm lazy.

public class PlayerOneGUI{
  private static final String FIFO_recv = "pipe_send";
  private static final String FIFO_send = "pipe_recv";

  private static JFrame frame = new JFrame();
  private static JPanel panel = new JPanel();
  private static JButton[][] buttons = new JButton[8][9];
  private static int[][] board = new int[8][9];
  private static JLabel indicator = new JLabel();

  private static String value_to_piece(int value){
    // This fuction translates a number value to a String
    // used in initializing the buttons in the panel
    if(Math.abs(value) == 15) return "NINJA";
    else if(Math.abs(value) == 14) return "FIV_S";
    else if(Math.abs(value) == 13) return "FOU_S";
    else if(Math.abs(value) == 12) return "THR_S";
    else if(Math.abs(value) == 11) return "TWO_S";
    else if(Math.abs(value) == 10) return "ONE_S";
    else if(Math.abs(value) == 9) return "COLON";
    else if(Math.abs(value) == 8) return "LTCOL";
    else if(Math.abs(value) == 7) return "MAJOR";
    else if(Math.abs(value) == 6) return "CAPTN";
    else if(Math.abs(value) == 5) return "FI_LT";
    else if(Math.abs(value) == 4) return "SE_LT";
    else if(Math.abs(value) == 3) return "SRGNT";
    else if(Math.abs(value) == 2) return "PRIVT";
    else if(Math.abs(value) == 1) return "BANNR";
    else return "     ";
  }

  private static void send(int sRow, int sCol, char sDir){
    // This function writes the row and col index of the piece moved
    // along with the direction it is moved ('w', 'a', 's', 'd')
    // This uses the pipe 'pipe_recv'
    System.out.println(sRow + " " + sCol + " " + sDir);
    BufferedWriter out = null;
    try {
      out = new BufferedWriter(new FileWriter(FIFO_send));
      out.write(sRow + "\n");
      out.write(sCol + "\n");
      out.write(sDir + "\n");
      out.close();
    }catch(IOException e){
      System.out.println("ERROR! IO send");
    }
  }

  private static void getBoard(){
    // This function reads numbers from 'pipe_send'
    // It then puts those numbers into 'board[][]' effectively creating
    // a copy of 'full_board' from the C program
    BufferedReader in = null;
    try {
      in = new BufferedReader(new FileReader(FIFO_recv));
      for(int q = 0; q < buttons.length; q++){
        for(int w = 0; w < buttons[q].length; w++){
          board[q][w] = Integer.parseInt(in.readLine());
        }
      }
      in.close();
    }catch(IOException e){
      System.out.println("ERROR! IO recieve");
    }
  }

  private static void addListeners(int q, int w){
    // This function initializes listeners for buttons so that when
    // user clicks it, it calls 'prepToSend()' or produce a sound that
    // will warn the user the he is trying to move a piece he has no
    // priveledge on
    final int row = q; // final variables are required if you're
    final int col = w; // going to use them inside annonymous listeners
    buttons[q][w].addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        if(board[q][w] > 0){
          prepToSend(row, col);
        } else {
          Toolkit.getDefaultToolkit().beep();
        }
      }
    });
  }

  private static void prepToSend(int row, int col){
    // This function handles majority of the processes in identifiying
    // user movement within the app
    // It temporarily disables the listeners installed by 'addListeners',
    // disables all the buttons around except the 4 buttons adjacent to
    // the button clicked, and then installs another listener to those
    // 4 buttons so when user clicks it, it calls 'send()'
    for(int q = 0; q < buttons.length; q++){
      for(int w = 0; w < buttons[q].length; w++){
        for(ActionListener removeThis : buttons[q][w].getActionListeners()) {
            buttons[q][w].removeActionListener(removeThis);
        }

        if (q == row-1 && w == col) { // tile in front of the selected tile
          if(board[q][w] <= 0)
            buttons[q][w].setBackground(Color.GREEN);
          else
            buttons[q][w].setBackground(Color.ORANGE);
          // add listener
          final int sRow = q;
          final int sCol = w;
          final char sDir = 'w';
          buttons[q][w].addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
              if(board[sRow][sCol] <= 0)
                send((sRow+1), sCol, sDir);
              else
                send(0, 0, 'z');
            }
          });
        } else if (q == row+1 && w == col) { // tile in back of the selected tile
          if(board[q][w] <= 0){
            buttons[q][w].setBackground(Color.GREEN);
          }else{
            buttons[q][w].setBackground(Color.ORANGE);
          }
          //add listener
          final int sRow = q;
          final int sCol = w;
          final char sDir = 's';
          buttons[q][w].addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
              if(board[sRow][sCol] <= 0)
                send((sRow-1), sCol, sDir);
              else
                send(0, 0, 'z');
            }
          });
        } else if (q == row && w == col-1) { // tile in left of the selected tile
          if(board[q][w] <= 0){
            buttons[q][w].setBackground(Color.GREEN);
          }else{
            buttons[q][w].setBackground(Color.ORANGE);
          }
          final int sRow = q;
          final int sCol = w;
          final char sDir = 'a';
          buttons[q][w].addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
              if(board[sRow][sCol] <= 0)
                send(sRow, (sCol+1), sDir);
              else
                send(0, 0, 'z');
            }
          });
        } else if (q == row && w == col+1) { // tile in right of the selected tile
          if(board[q][w] <= 0){
            buttons[q][w].setBackground(Color.GREEN);
          }else{
            buttons[q][w].setBackground(Color.ORANGE);
          }
          final int sRow = q;
          final int sCol = w;
          final char sDir = 'd';
          buttons[q][w].addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
              if(board[sRow][sCol] <= 0)
                send(sRow, (sCol-1), sDir);
              else
                send(0, 0, 'z');
            }
          });
        } else {
          buttons[q][w].setEnabled(false);
        }
      }
    }
  }

  private static void updatePanelListen(){
    // This function redraws the panel and listens for user input
    // This function is used when its this player's turn
    // It interfaces with 'value_to_piece()' to draw the text inside
    // the buttons. It also 'addListeners()' to those buttons
    panel.removeAll();
    for(int q = 0; q < buttons.length; q++){
      for(int w = 0; w < buttons[q].length; w++){
        buttons[q][w] = new JButton();
        buttons[q][w].setPreferredSize(new Dimension(100, 40));
        buttons[q][w].setFocusPainted(false);

        buttons[q][w].setFont(new Font(Font.SANS_SERIF, Font.BOLD, 15));
        buttons[q][w].setForeground(Color.WHITE);
        if(board[q][w] > 0){
          buttons[q][w].setText(value_to_piece(board[q][w]));
          buttons[q][w].setBackground(Color.BLUE);
        } else if(board[q][w] < 0){
          buttons[q][w].setText("*****");
          buttons[q][w].setBackground(Color.RED);
        } else {
          buttons[q][w].setBackground(Color.WHITE);
        }

        addListeners(q, w);

        panel.add(buttons[q][w]);
      }
    }
    indicator.setText("Your turn..");
    indicator.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 18));
    indicator.setForeground(Color.GREEN);
    panel.add(indicator);
    panel.revalidate();
    panel.repaint();
  }

  private static void updatePanelWait(){
    // This function redraws the panel to show the movement that the
    // oppenent did
    // This function is used when its not this player's turn
    // It interfaces with 'value_to_piece()' to draw the text inside
    // the buttons. It does not 'addListeners()' because its sole purpose
    // is to display changes
    panel.removeAll();
    for(int q = 0; q < buttons.length; q++){
      for(int w = 0; w < buttons[q].length; w++){

        buttons[q][w] = new JButton();
        buttons[q][w].setPreferredSize(new Dimension(100, 40));
        buttons[q][w].setFocusPainted(false);

        buttons[q][w].setFont(new Font(Font.SANS_SERIF, Font.BOLD, 15));
        buttons[q][w].setForeground(Color.WHITE);
        if(board[q][w] > 0){
          buttons[q][w].setText(value_to_piece(board[q][w]));
          buttons[q][w].setBackground(Color.BLUE);
        } else if(board[q][w] < 0){
          buttons[q][w].setText("*****");
          buttons[q][w].setBackground(Color.RED);
        } else {
          buttons[q][w].setBackground(Color.WHITE);
        }

        // addListeners(q, w);

        panel.add(buttons[q][w]);
      }
    }
    indicator.setText("Opponents' turn... Please Wait...");
    indicator.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 18));
    indicator.setForeground(Color.RED);
    panel.add(indicator);
    panel.revalidate();
    panel.repaint();
  }


  public static void main(String[] fudge){
    frame.setTitle("GAME OF THE LOCAL AREA GENERALS (GLAG) [Player1]");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    panel.setPreferredSize(new Dimension(950, 400));
    panel.setBackground(Color.WHITE);
    indicator.setText("Waiting for a client...");

    panel.add(indicator);
    frame.add(panel);
    frame.pack();
    frame.setResizable(false);
    frame.setLocationRelativeTo(null);
    frame.setVisible(true);

    while(true){
      getBoard();

      updatePanelListen();

      getBoard();

      updatePanelWait();
    }
  }
}
