#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <stdbool.h>
#include <ctype.h>

void die_with_error(char *error_msg){
  printf("%s", error_msg);
  exit(-1);
}

int piece_to_value(char str[6]){
  // This function translates a string input into a corresponding int
  // Player1 recieves positive numbers while Player2 recieves negatives
  // This funtion is used when generating this player's 'halfboard'
  if(strcmp(str, "NINJA") == 0) return -15;
  else if(strcmp(str, "FIV_S") == 0) return -14;
  else if(strcmp(str, "FOU_S") == 0) return -13;
  else if(strcmp(str, "THR_S") == 0) return -12;
  else if(strcmp(str, "TWO_S") == 0) return -11;
  else if(strcmp(str, "ONE_S") == 0) return -10;
  else if(strcmp(str, "COLON") == 0) return -9;
  else if(strcmp(str, "LTCOL") == 0) return -8;
  else if(strcmp(str, "MAJOR") == 0) return -7;
  else if(strcmp(str, "CAPTN") == 0) return -6;
  else if(strcmp(str, "FI_LT") == 0) return -5;
  else if(strcmp(str, "SE_LT") == 0) return -4;
  else if(strcmp(str, "SRGNT") == 0) return -3;
  else if(strcmp(str, "PRIVT") == 0) return -2;
  else if(strcmp(str, "BANNR") == 0) return -1;
  else return 0;
}

int collision(int attacker, int defender){
  // this function returns +1 if attacker wins
  // -1 if attacker lose and 0 if draw based on the
  // rules applied in Game of the Generals
  attacker = abs(attacker);
  defender = abs(defender);

  if(attacker == 15 && defender == 2 || attacker == 2 && defender == 15){
    return (attacker == 15) ? -1 : +1;
  } else if(attacker == 1 && defender == 1){
    return +1;
  } else {
    if(attacker > defender){
      return +1;
    } else if(attacker < defender){
      return -1;
    } else {
      return 0;
    }
  }
}

void generate_halfboard(int halfboard[4][9]){
  // This function reads 'set-army.txt' file to generate this player's
  // board layout. It passes the strings it reads to 'piece_to_value()'
  // and recieves a number that it will then put in the cells of
  // 'halfboard[][]'
  char piece[6];

  FILE *file;
  file = fopen("../../set-army.txt", "r");
  for(int q = 0; q < 4; q++){
    for(int w = 0; w < 9; w++){
      if(q == 0) halfboard[q][w] = 0;
      else{
        fscanf(file, "%s", &piece[0]);
        halfboard[q][w] = piece_to_value(piece);
      }
    }
  }
  fclose(file);
}

void display_board(int full_board[8][9]){
  // this function writes the 'full_board[][]' into the 'pipe_send'
  // so the java program can read and display it. **in reverse**
  char* outFile = "pipe_send";
  FILE *fp_out = fopen(outFile, "w");

  for(int q = 0; q < 8; q++){
    for(int w = 0; w < 9; w++){
      fprintf(fp_out, "%d\n", full_board[7-q][8-w]);
    }
  }

  fclose(fp_out);
}

void player_two_turn(int full_board[8][9]){
  // this function "moves the pieces around the full_board[][]"
  // it first reads 'pipe_recv' to get the the player's move [written
  // by the java program]
  char* outFile = "pipe_recv";
  int row, col;
  char dir;

  FILE *fp_in = fopen(outFile, "r");
  fscanf(fp_in, "%d\n", &row);
  fscanf(fp_in, "%d\n", &col);
  fscanf(fp_in, "%c\n", &dir);
  fclose(fp_in);

  if(tolower(dir) == 'w'){
    if(full_board[row+1][col] != 0){ // if defender != 0
      if(full_board[row+1][col] > 0){ // if defender not teammate
        if(collision(full_board[row][col], full_board[row+1][col]) == +1){ // attacker wins
          full_board[row+1][col] = full_board[row][col];
          full_board[row][col] = 0;
        } else if(collision(full_board[row][col], full_board[row+1][col]) == -1) { // if attacker lose
          full_board[row][col] = 0;
        } else { // if draw
          full_board[row+1][col] = 0;
          full_board[row][col] = 0;
        }
      }
    } else { // if defender is balnk space
      full_board[row+1][col] = full_board[row][col];
      full_board[row][col] = 0;
    }
  }
  else if(tolower(dir) == 's'){
    if(full_board[row-1][col] != 0){ // if defender != 0
      if(full_board[row-1][col] > 0){ // if defender not teammate
        if(collision(full_board[row][col], full_board[row-1][col]) == +1){ // attacker wins
          full_board[row-1][col] = full_board[row][col];
          full_board[row][col] = 0;
        } else if(collision(full_board[row][col], full_board[row-1][col]) == -1) { // if attacker lose
          full_board[row][col] = 0;
        } else { // if draw
          full_board[row-1][col] = 0;
          full_board[row][col] = 0;
        }
      }
    } else { // if defender is balnk space
      full_board[row-1][col] = full_board[row][col];
      full_board[row][col] = 0;
    }
  }
  else if(tolower(dir) == 'd'){
    if(full_board[row][col-1] != 0){ // if defender != 0
      if(full_board[row][col-1] > 0){ // if defender not teammate
        if(collision(full_board[row][col], full_board[row][col-1]) == +1){ // attacker wins
          full_board[row][col-1] = full_board[row][col];
          full_board[row][col] = 0;
        } else if(collision(full_board[row][col], full_board[row][col-1]) == +1) { // if attacker lose
          full_board[row][col] = 0;
        } else { // if draw
          full_board[row][col-1] = 0;
          full_board[row][col] = 0;
        }
      }
    } else { // if defender is balnk space
      full_board[row][col-1] = full_board[row][col];
      full_board[row][col] = 0;
    }
  }
  else if(tolower(dir) == 'a'){
    if(full_board[row][col+1] != 0){ // if defender != 0
      if(full_board[row][col+1] > 0){ // if defender not teammate
        if(collision(full_board[row][col], full_board[row][col+1]) == +1){ // attacker wins
          full_board[row][col+1] = full_board[row][col];
          full_board[row][col] = 0;
        } else if(collision(full_board[row][col], full_board[row][col+1]) == -1) { // if attacker lose
          full_board[row][col] = 0;
        } else { // if draw
          full_board[row][col+1] = 0;
          full_board[row][col] = 0;
        }
      }
    } else { // if defender is balnk space
      full_board[row][col+1] = full_board[row][col];
      full_board[row][col] = 0;
    }
  }
  else {
    printf("%s\n", "Please use W, A, S, D in specifying directions..");
    // player_two_turn(full_board);
  }
}

bool game_over(int full_board[8][9]){
  // this function returs true if it detects one of the BANNRs is
  // missing or if a BANNR reached the last row on the opposite side
  bool reached_base_row = false;
  for(int q = 0; q < 9; q++)
    if(full_board[0][q] == 1 || full_board[7][q] == -1)
      reached_base_row = true;

  bool bannrOne = true;
  bool bannrTwo = true;
  for(int q = 0; q < 8; q++){
    for(int w = 0; w < 9; w++){
      if(full_board[q][w] == 1)
      bannrOne = false;
      if(full_board[q][w] == -1)
      bannrTwo = false;
    }
  }

  return bannrOne || bannrTwo || reached_base_row;
}

int getwinner(int full_board[8][9]){
  // this function returns 0 by default; 1 if player1 won the game;
  // and 2 if player2 won the game
  for(int q = 0; q < 9; q++){
    if(full_board[0][q] == 1)
      return 1;
    if(full_board[7][q] == -1)
      return 2;
  }

  bool player1 = false;
  bool player2 = false;
  for(int q = 0; q < 8; q++){
    for(int w = 0; w < 9; w++){
      if(full_board[q][w] == 1)
        player1 = true;
      if(full_board[q][w] == -1)
        player2 = true;
    }
  }

  if(player1 == false)
    return 2;
  if(player2 == false)
    return 1;

  return 0;
}


int main(int fudgec,  char *fudgev[]){
  int client_sock,  port_no,  n;
  struct sockaddr_in server_addr;
  struct hostent *server;
  // Create a socket using TCP
  client_sock = socket(AF_INET,  SOCK_STREAM,  0);
  if (client_sock < 0) die_with_error("Error: socket() Failed.");
  //printf("Looking for host '%s'...\n", argv[1]);
  server = gethostbyname(fudgev[1]);
  if (server == NULL) die_with_error("Error: No such host.");
  //printf("Host found!\n");
  // Establish a connection to server
  port_no = 50001;
  bzero((char *) &server_addr,  sizeof(server_addr));
  server_addr.sin_family = AF_INET;
  bcopy((char *)server->h_addr, (char *)&server_addr.sin_addr.s_addr, server->h_length);
  server_addr.sin_port = htons(port_no);
  //printf("Connecting to server at port %d...\n", port_no);
  if (connect(client_sock, (struct sockaddr *) &server_addr, sizeof(server_addr)) < 0)
  die_with_error("Error: connect() Failed.");

  //######################################################## get army setup for player two
  int halfboard[4][9];
  generate_halfboard(halfboard);
  printf("Player 2 board generated\n");

  //######################################################## send army setup to player one
  for(int q = 0; q < 4; q++){
    for(int w = 0; w < 9; w++){
      n = send(client_sock, &halfboard[3-q][8-w], sizeof(halfboard[4-q][9-w]), 0);
    }
  }
  printf("Player 2 board sent\n");

  //######################################################## recieve full board
  int full_board[8][9];
  for(int q = 0; q < 8; q++){
    for(int w = 0; w < 9; w++){
      n = recv(client_sock, &full_board[q][w], sizeof(full_board[q][w]), 0);
    }
  }
  printf("Full board generated\n");

  //######################################################## GAME STARTS
  while(1){
    display_board(full_board);

    printf("%s\n", "recieving");
    for(int q = 0; q < 8; q++){
      for(int w = 0; w < 9; w++){
        n = recv(client_sock, &full_board[q][w], sizeof(full_board[q][w]), 0);
      }
    }

    if(game_over(full_board)){
      printf("%s\n", "Game Over... Player One Won!");
      break;
    }

    display_board(full_board);

    player_two_turn(full_board);

    for(int q = 0; q < 8; q++){
      for(int w = 0; w < 9; w++){
        n = send(client_sock, &full_board[q][w], sizeof(halfboard[q][w]), 0);
      }
    }

    if(game_over(full_board)){
      printf("%s\n", "Game Over... Player Two Won!");
      break;
    }
  }

  close(client_sock);
  return getwinner(full_board);
}
