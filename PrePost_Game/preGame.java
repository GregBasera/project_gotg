import java.io.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

//huhuhu
public class preGame{
  private static JButton[] buttonsReff = new JButton[21]; //all pieces
  private static String[] refference =
  {"NINJA", "NINJA", "FIV_S", "FOU_S", "THR_S", "TWO_S", "ONE_S", "COLON", "LTCOL",
   "MAJOR", "CAPTN", "FI_LT", "SE_LT", "SRGNT", "PRIVT", "PRIVT", "PRIVT",
   "PRIVT", "PRIVT", "PRIVT", "BANNR"};
  private static JButton[][] buttonFinalLayout = new JButton[3][9];
  private static String[][] finalLayout = new String[3][9];
  private static JFrame frame = new JFrame();
  private static JPanel panel = new JPanel();
  // private static JPanel refPanel = new JPanel();
  private static JTextArea indicator = new JTextArea();
  private static JLabel topIndicator = new JLabel();
  private static JButton genButton = new JButton();
  static boolean isPressed = false;
  static boolean isFilled[][] = new boolean[3][9];
  static String hold = null;

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//Final Layout

  private static void finalLay(int player){
    topIndicator.setText("-----THE ENEMY WILL APPEAR ON THIS SIDE-----");
    topIndicator.setHorizontalAlignment(SwingConstants.CENTER);
    topIndicator.setPreferredSize(new Dimension(950, 20));
    panel.add(topIndicator);


    for(int q = 0; q < buttonFinalLayout.length; q++){
      for(int w = 0; w < buttonFinalLayout[q].length; w++){
        int row = q;
        int col = w;
        buttonFinalLayout[q][w] = new JButton("-----");
        buttonFinalLayout[q][w].setForeground(Color.WHITE);
        buttonFinalLayout[q][w].setBackground(Color.WHITE);
        buttonFinalLayout[q][w].setPreferredSize(new Dimension(100, 40));
        buttonFinalLayout[q][w].setFocusPainted(false);
        finalLayout[q][w] = "-----";

        buttonFinalLayout[q][w].addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent e) {
            finalListeners(row, col, player);
            // System.out.println(e.getActionCommand() + " pressed.");
          }
        });


        panel.add(buttonFinalLayout[q][w]);
        indicator.setText("Place the pieces below on the slots above.\n" +
        "The layout made here will appear as your army's layout on the game proper.\n" +
        "Press PLAY! when ready.");
        // indicator.setHorizontalAlignment(SwingConstants.CENTER);
        indicator.setPreferredSize(new Dimension(720, 60));
        indicator.setFont(new Font("Segoe Script", Font.BOLD, 15));
        indicator.setForeground(Color.RED);
        panel.add(indicator);
      }
    }
    genButton = new JButton("PLAY!");
    genButton.setBackground(Color.GRAY);
    genButton.setPreferredSize(new Dimension(150, 40));
    genButton.setFocusPainted(false);
    genButton.setEnabled(false);

    genButton.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e){
          // System.out.println();
          // for(int i = 0; i < finalLayout.length; i++){
          //   for(int o = 0; o < finalLayout[i].length; o++){
          //     System.out.printf("%s ", finalLayout[i][o]);
          //
          //     if(o == finalLayout[i].length - 1)
          //       System.out.println();
          //   }
          // }
          try{
            genListener(finalLayout);
          } catch(IOException ex){
            ex.printStackTrace();
          }
          System.exit(0);
        }
      });

    panel.add(genButton);
  }

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//FinalLayout Listener

  private static void finalListeners(int q, int w, int player){
    int row = q;
    int col = w;
    String hold2 = null;
    // buttonFinalLayout[q][w].setText( <string from temp var> )
    // buttonFinalLayout[q][w].setBackground( <color of the player> )
    // finalLayout[q][w] = <string from temp var>
    // destroy refference button
    // repaint panel
    if(q == row && w == col){
      if(isPressed == false && isFilled[row][col] == false && hold == null){
        Toolkit.getDefaultToolkit().beep();
      } else if(isPressed == true && isFilled[row][col] == false && hold != null){
        buttonFinalLayout[row][col].setBackground((player == 1) ? Color.BLUE : Color.RED);
        // buttonFinalLayout[row][col].setBackground(Color.BLUE);
        buttonFinalLayout[row][col].setText(hold);
        finalLayout[row][col] = hold;
        // System.out.printf("%s%n", finalLayout[row][col]);
        isFilled[row][col] = true;
        isPressed = false;
        hold = null;
      } else if(isFilled[row][col] == true && hold == null){//hold != null
        isPressed = true;
        hold = buttonFinalLayout[row][col].getText();
        buttonFinalLayout[row][col].setBackground(Color.WHITE);
        buttonFinalLayout[row][col].setText("-----");
        finalLayout[row][col] = "-----";
        isFilled[row][col] = false;
      } else if(isFilled[row][col] == true && hold != null){
          isPressed = true;
          hold2 = buttonFinalLayout[row][col].getText();
          buttonFinalLayout[row][col].setText(hold);
          finalLayout[row][col] = hold;
          // System.out.printf("%n%s%n", hold2);
          hold = hold2;
      }
    }
    // add listener... when clicked get the string from the temporary variable
    int in = 0;
    for(int i = 0 ; i < buttonsReff.length; i++){
      if(buttonsReff[i].getText() == null){ // should be disabled if holding something && when rehold
        in += 1;
        if(in == buttonsReff.length && hold == null && hold2 == null){
          genButton.setEnabled(true);
          genButton.setBackground(Color.GREEN);
        }
        else{
          genButton.setEnabled(false);
          genButton.setBackground(Color.GRAY);
        }
      }
    }
  }

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//Generate Button

private static void genListener(String[][] board) throws IOException{
    String[][] boardLay = board;
    BufferedWriter boardLayout = null;
    boardLayout = new BufferedWriter(new FileWriter("../set-army.txt", false));
    //try {
      for(int q = 0; q < finalLayout.length; q++){
        for(int w = 0; w < finalLayout[q].length; w++){
          boardLayout.write(boardLay[q][w] + " ");
          if(w == finalLayout[q].length - 1 && q < finalLayout.length-1)
            boardLayout.newLine();
        }
        // boardLayout.newLine();
      }
      boardLayout.close();
  }

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//Reference Layout

private static void refLayout(){
  for(int q = 0; q < refference.length; q++){
    int row = q;
    buttonsReff[q] = new JButton(refference[q]);
    buttonsReff[q].setBackground(Color.WHITE);
    buttonsReff[q].setPreferredSize(new Dimension(100, 40));
    buttonsReff[q].setFocusPainted(false);

    buttonsReff[q].addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e){
        refListener(row);
        // System.out.println(e.getActionCommand() + " pressed.");
      }
    });
    panel.add(buttonsReff[q]);

  }
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
//Reference Listener

  private static void refListener(int q){
    int row = q;
    int in = 0;
    boolean isGood = false;
    // add listener... when clicked change the background color(highlight)
    // copy string from 'refference[]' to a temporary variable
    if(q == row && isPressed == false){
      buttonsReff[row].setBackground(Color.GRAY);
      hold = refference[row];
      // System.out.printf("Holding: %s%n", hold);
      isPressed = true;
      buttonsReff[row].setText(null);
      buttonsReff[row].setEnabled(false);
    } else if(q == row && isPressed == true){
        Toolkit.getDefaultToolkit().beep();
    }
  }



  public static void main(String[] args){
  frame.setTitle("GLAG PRE-GAME");
  frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  panel.setPreferredSize(new Dimension(950, 400));
  panel.setBackground(Color.WHITE);

  finalLay(Integer.valueOf(args[0]));
  refLayout();

  frame.add(panel);
  frame.pack();
  frame.setResizable(false);
  frame.setLocationRelativeTo(null);
  frame.setVisible(true);

  }
}
