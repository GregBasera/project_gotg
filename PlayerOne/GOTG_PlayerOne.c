
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <stdbool.h>
#include <ctype.h>

void die_with_error(char *error_msg){
  printf("%s", error_msg);
  exit(-1);
}

int piece_to_value(char str[6]){
  if(strcmp(str, "NINJA") == 0) return 15;
  else if(strcmp(str, "FIV_S") == 0) return 14;
  else if(strcmp(str, "FOU_S") == 0) return 13;
  else if(strcmp(str, "THR_S") == 0) return 12;
  else if(strcmp(str, "TWO_S") == 0) return 11;
  else if(strcmp(str, "ONE_S") == 0) return 10;
  else if(strcmp(str, "COLON") == 0) return 9;
  else if(strcmp(str, "LTCOL") == 0) return 8;
  else if(strcmp(str, "MAJOR") == 0) return 7;
  else if(strcmp(str, "CAPTN") == 0) return 6;
  else if(strcmp(str, "FI_LT") == 0) return 5;
  else if(strcmp(str, "SE_LT") == 0) return 4;
  else if(strcmp(str, "SRGNT") == 0) return 3;
  else if(strcmp(str, "PRIVT") == 0) return 2;
  else if(strcmp(str, "BANNR") == 0) return 1;
  else return 0;
}

char* value_to_piece(int value){
  if(abs(value) == 15) return "NINJA";
  else if(abs(value) == 14) return "FIV_S";
  else if(abs(value) == 13) return "FOU_S";
  else if(abs(value) == 12) return "THR_S";
  else if(abs(value) == 11) return "TWO_S";
  else if(abs(value) == 10) return "ONE_S";
  else if(abs(value) == 9) return "COLON";
  else if(abs(value) == 8) return "LTCOL";
  else if(abs(value) == 7) return "MAJOR";
  else if(abs(value) == 6) return "CAPTN";
  else if(abs(value) == 5) return "FI_LT";
  else if(abs(value) == 4) return "SE_LT";
  else if(abs(value) == 3) return "SRGNT";
  else if(abs(value) == 2) return "PRIVT";
  else if(abs(value) == 1) return "BANNR";
  else return "     ";
}

int row_translate(char row){
  if(tolower(row) == 'a') return 0;
  else if(tolower(row) == 'b') return 1;
  else if(tolower(row) == 'c') return 2;
  else if(tolower(row) == 'd') return 3;
  else if(tolower(row) == 'e') return 4;
  else if(tolower(row) == 'f') return 5;
  else if(tolower(row) == 'g') return 6;
  else if(tolower(row) == 'h') return 7;
  else return -1;
}

int collision(int attacker, int defender){
  // this function returns +1 if attacker wins
  // -1 if attacker lose and 0 if draw based on the
  // rules applied in Game of the Generals
  attacker = abs(attacker);
  defender = abs(defender);

  if(attacker == 15 && defender == 2 || attacker == 2 && defender == 15){
    return (attacker == 15) ? -1 : +1;
  } else if(attacker == 1 && defender == 1){
    return +1;
  } else {
    if(attacker > defender){
      return +1;
    } else if(attacker < defender){
      return -1;
    } else {
      return 0;
    }
  }
}

void generate_halfboard(int halfboard[4][9]){
  char piece[6];

  FILE *file;
  file = fopen("../set-army.txt", "r");
  for(int q = 0; q < 4; q++){
    for(int w = 0; w < 9; w++){
      if(q == 0) halfboard[q][w] = 0;
      else{
        fscanf(file, "%s", &piece[0]);
        halfboard[q][w] = piece_to_value(piece);
      }
    }
  }
  fclose(file);
}

void display_board(int full_board[8][9]){
  char row[8] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'};
  int col[9] = {1, 2, 3, 4, 5, 6, 7, 8, 9};

  system("clear");
  printf("\n    ");
  for(int q = 0; q < 9; q++) {printf("\033[0;33m"); printf("   %d    ", col[q]); printf("\033[0m");}
  printf("\n   +");
  for(int q = 0; q < 9; q++) printf("%s", "-------+");
  printf("\n");
  for(int q = 0; q < 8; q++){
    printf("\033[0;33m");     // YELLOW
    printf(" %c ", row[q]);
    printf("\033[0m");        // RESET
    printf("|");
    for(int w = 0; w < 9; w++){
      if(full_board[q][w] < 0){
        printf("\033[1;31m");     // RED
        printf("%7s", " ***** ");
        printf("\033[0m");        // RESET
      } else if(full_board[q][w] == 0){
        printf(" %5s ", value_to_piece(full_board[q][w]));
      } else {
        printf("\033[0;34m");     // BLUE
        printf(" %5s ", value_to_piece(full_board[q][w]));
        printf("\033[0m");        // RESET
      }
      printf("|");
    }
    printf("\n   +");
    for(int q = 0; q < 9; q++) printf("%s", "-------+");
    printf("\n");
  }
}

void player_one_turn(int full_board[8][9]){
  char row, dir;
  int col;

  printf("\033[0;36m");     //CYAN
  printf("\n%s", "Your move> ");
  printf("\033[0m");        // RESET

  scanf("%c%d %c", &row, &col, &dir);
  while ((getchar()) != '\n');

  if(row_translate(row) == -1){
    printf("%s\n", "Invalid move");
    player_one_turn(full_board);
  }
  else if(full_board[row_translate(row)][col-1] < 0){
    printf("%s\n", "That is not your piece..");
    player_one_turn(full_board);
  }
  else {
    if(tolower(dir) == 'w'){
      if(full_board[row_translate(row)-1][col-1] != 0){ // if defender != 0
        if(full_board[row_translate(row)-1][col-1] < 0){ // if defender not teammate
          if(collision(full_board[row_translate(row)][col-1], full_board[row_translate(row)-1][col-1]) == +1){ // attacker wins
            full_board[row_translate(row)-1][col-1] = full_board[row_translate(row)][col-1];
            full_board[row_translate(row)][col-1] = 0;
          } else if(collision(full_board[row_translate(row)][col-1], full_board[row_translate(row)-1][col-1]) == -1) { // if attacker lose
            full_board[row_translate(row)][col-1] = 0;
          } else { // if draw
            full_board[row_translate(row)-1][col-1] = 0;
            full_board[row_translate(row)][col-1] = 0;
          }
        } else { //if defender is teammate
          printf("%s\n", "Griefing!");
          player_one_turn(full_board);
        }
      } else { // if defender is balnk space
        full_board[row_translate(row)-1][col-1] = full_board[row_translate(row)][col-1];
        full_board[row_translate(row)][col-1] = 0;
      }
    }
    else if(tolower(dir) == 's'){
      if(full_board[row_translate(row)+1][col-1] != 0){ // if defender != 0
        if(full_board[row_translate(row)+1][col-1] < 0){ // if defender not teammate
          if(collision(full_board[row_translate(row)][col-1], full_board[row_translate(row)+1][col-1]) == +1){ // attacker wins
            full_board[row_translate(row)+1][col-1] = full_board[row_translate(row)][col-1];
            full_board[row_translate(row)][col-1] = 0;
          } else if(collision(full_board[row_translate(row)][col-1], full_board[row_translate(row)+1][col-1]) == -1) { // if attacker lose
            full_board[row_translate(row)][col-1] = 0;
          } else { // if draw
            full_board[row_translate(row)+1][col-1] = 0;
            full_board[row_translate(row)][col-1] = 0;
          }
        } else { //if defender is teammate
          printf("%s\n", "Griefing!");
          player_one_turn(full_board);
        }
      } else { // if defender is balnk space
        full_board[row_translate(row)+1][col-1] = full_board[row_translate(row)][col-1];
        full_board[row_translate(row)][col-1] = 0;
      }
    }
    else if(tolower(dir) == 'd'){
      if(full_board[row_translate(row)][col] != 0){ // if defender != 0
        if(full_board[row_translate(row)][col] < 0){ // if defender not teammate
          if(collision(full_board[row_translate(row)][col-1], full_board[row_translate(row)][col]) == +1){ // attacker wins
            full_board[row_translate(row)][col] = full_board[row_translate(row)][col-1];
            full_board[row_translate(row)][col-1] = 0;
          } else if(collision(full_board[row_translate(row)][col-1], full_board[row_translate(row)][col]) == -1) { // if attacker lose
            full_board[row_translate(row)][col-1] = 0;
          } else { // if draw
            full_board[row_translate(row)][col] = 0;
            full_board[row_translate(row)][col-1] = 0;
          }
        } else { //if defender is teammate
          printf("%s\n", "Griefing!");
          player_one_turn(full_board);
        }
      } else { // if defender is balnk space
        full_board[row_translate(row)][col] = full_board[row_translate(row)][col-1];
        full_board[row_translate(row)][col-1] = 0;
      }
    }
    else if(tolower(dir) == 'a'){
      if(full_board[row_translate(row)][col-2] != 0){ // if defender != 0
        if(full_board[row_translate(row)][col-2] < 0){ // if defender not teammate
          if(collision(full_board[row_translate(row)][col-1], full_board[row_translate(row)][col-2]) == +1){ // attacker wins
            full_board[row_translate(row)][col-2] = full_board[row_translate(row)][col-1];
            full_board[row_translate(row)][col-1] = 0;
          } else if(collision(full_board[row_translate(row)][col-1], full_board[row_translate(row)][col-2]) == -1) { // if attacker lose
            full_board[row_translate(row)][col-1] = 0;
          } else { // if draw
            full_board[row_translate(row)][col-2] = 0;
            full_board[row_translate(row)][col-1] = 0;
          }
        } else { //if defender is teammate
          printf("%s\n", "Griefing!");
          player_one_turn(full_board);
        }
      } else { // if defender is balnk space
        full_board[row_translate(row)][col-2] = full_board[row_translate(row)][col-1];
        full_board[row_translate(row)][col-1] = 0;
      }
    }
    else {
      printf("%s\n", "Please use W, A, S, D in specifying directions..");
      player_one_turn(full_board);
    }
  }
}

bool game_over(int full_board[8][9]){
  bool reached_base_row = false;
  for(int q = 0; q < 9; q++)
    if(full_board[0][q] == 1 || full_board[7][q] == -1)
      reached_base_row = true;

  bool bannrOne = true;
  bool bannrTwo = true;
  for(int q = 0; q < 8; q++){
    for(int w = 0; w < 9; w++){
      if(full_board[q][w] == 1)
      bannrOne = false;
      if(full_board[q][w] == -1)
      bannrTwo = false;
    }
  }

  return bannrOne || bannrTwo || reached_base_row;
}


int main(int argc, char *argv[]){
  int server_sock, client_sock, port_no, client_size, n;
  // char buffer[256];
  struct sockaddr_in server_addr, client_addr;
  // if (argc < 2) {
  //   printf("Usage: %s port_no", argv[0]);
  //   exit(1);
  // }

  // Create a socket for incoming connections
  server_sock = socket(AF_INET, SOCK_STREAM, 0);
  if (server_sock < 0) die_with_error("Error: socket() Failed.");

  // Bind socket to a port
  bzero((char *) &server_addr, sizeof(server_addr));
  port_no = 50001;
  server_addr.sin_family = AF_INET; // Internet address
  server_addr.sin_addr.s_addr = INADDR_ANY; // Any incoming interface
  server_addr.sin_port = htons(port_no); // Local port
  if (bind(server_sock, (struct sockaddr *) &server_addr, sizeof(server_addr)) < 0) die_with_error("Error: bind() Failed.");

  // Mark the socket so it will listen for incoming connections
  listen(server_sock, 5);

  // Accept new connection
  client_size = sizeof(client_addr);
  client_sock = accept(server_sock, (struct sockaddr *) &client_addr, &client_size);
  if (client_sock < 0) die_with_error("Error: accept() Failed.");

  //######################################################## get army setup for player one
  int halfboard[4][9];
  generate_halfboard(halfboard);
  printf("Player 1 board generated\n");

  //######################################################## recieve army setup from player two and generate the fullboard
  int full_board[8][9];

  for(int q = 0; q < 8; q++){
    for(int w = 0; w < 9; w++){
      if(q < 4){
        n = recv(client_sock, &full_board[q][w], sizeof(full_board[q][w]), 0);
        n = send(client_sock, &n, sizeof(n), 0);
      } else {
        full_board[q][w] = halfboard[q-4][w];
      }
    }
  }
  printf("Player 2 board recieved\n");

  int flag;
  for(int q = 0; q < 8; q++){
    for(int w = 0; w < 9; w++){
      n = send(client_sock, &full_board[q][w], sizeof(halfboard[q][w]), 0);
      n = recv(client_sock, &flag, sizeof(flag), 0);
      if(flag < 1) break;
    }
  }
  printf("Full board generated\n");

  //######################################################## GAME STARTS
  while(1){
    display_board(full_board);

    player_one_turn(full_board);

    for(int q = 0; q < 8; q++){
      for(int w = 0; w < 9; w++){
        n = send(client_sock, &full_board[q][w], sizeof(halfboard[q][w]), 0);
      }
    }

    if(game_over(full_board)){
      printf("%s\n", "Game Over... Player One Won!");
      close(client_sock);
      close(server_sock);
      break;
    }

    display_board(full_board);

    printf("\033[0;36m");     //CYAN
    printf("\n%s", "Opponents' turn... please wait... ");
    printf("\033[0m");        // RESET

    printf("%s\n", "recieving");
    for(int q = 0; q < 8; q++){
      for(int w = 0; w < 9; w++){
        n = recv(client_sock, &full_board[q][w], sizeof(full_board[q][w]), 0);
      }
    }

    if(game_over(full_board)){
      printf("%s\n", "Game Over... Player Two Won!");
      close(client_sock);
      close(server_sock);
      break;
    }
  }

  return 0;
}
