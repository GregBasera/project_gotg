#!/bin/bash

clear
echo "GLAG Starting..."
echo ""
echo "Hello! Welcome to"

echo "
____/%%%%%%%%%%%\__/%%\_________________/%%%%%%%%\________/%%%%%%%%%%%\_
___/%%\//////////__\/%%\_______________/%%%%%%%%%%%%\____/%%\//////////__
 __/%%\_____________\/%%\______________/%%\/////////%%\__/%%\_____________
  _\/%%\____/%%%%%%\_\/%%\_____________\/%%\_______\/%%\_\/%%\____/%%%%%%\_
   _\/%%\___\/////%%\_\/%%\_____________\/%%%%%%%%%%%%%%\_\/%%\___\/////%%\_
    _\/%%\_______\/%%\_\/%%\_____________\/%%\/////////%%\_\/%%\_______\/%%\_
     _\/%%\_______\/%%\_\/%%\_____________\/%%\_______\/%%\_\/%%\_______\/%%\_
      _\//%%%%%%%%%%%%/__\/%%%%%%%%%%%%%%\_\/%%\_______\/%%\_\//%%%%%%%%%%%%/__
       __\////////////____\///////////////__\///________\///___\////////////____"
echo -e "\e[5m\n       GAME        OF        THE        LOCAL        AREA        GENERALS\e[25m"


printf "\n\n%s" "Are you the server? [y|n]: "
read -r get
if [[ ${get^} = 'Y' ]]; then
  printf '%s\n' 'Server...'

  # Army Setup
  printf '\n%s' 'Use previous army setup? [y|n]: '
  read -r setup
  if [[ ${setup^} = 'N' ]]; then
    echo "Loading pre-game..."
    chmod 700 set-army.txt # read-write-execute
    cd PrePost_Game
    java preGame 1
    cd ..
    chmod 444 set-army.txt # read-only
  fi

  cd GUI_imple/PlayerOne
  # Establish pipes on PlayerOne
  mkfifo pipe_send
  mkfifo pipe_recv
  # run GUI and push it in the background
  java PlayerOneGUI &
  # PID of GUI so we can kill it when game is terminated
  GUI=$!
  # run GAME
  echo "Running server.. waiting for a client..."
  ./server
  # store the exit status of the GAME
  exitstatus=$?
  # run 'postGame' based on the exit status of the GAME
  cd ../../PrePost_Game
  if [[ $exitstatus = 1 ]]; then
    java postGame 1
  elif [[ $exitstatus = 2 ]]; then
    java postGame 0
  else
    java postGame -1
  fi

  kill $GUI

  # Destroy pipes on PlayerOne
  cd ../GUI_imple/PlayerOne
  rm pipe_send
  rm pipe_recv
  cd ../../
else
  printf '%s\n' 'Client...'
  printf '%s' 'IPv4 address of the Server: '
  read -r ip

  # Army Setup
  printf '\n%s' 'Use previous army setup? [y|n]: '
  read -r setup
  if [[ ${setup^} = 'N' ]]; then
    echo "Loading pre-game..."
    chmod 700 set-army.txt # read-write-execute
    cd PrePost_Game
    java preGame 2
    cd ..
    chmod 444 set-army.txt # read-only
  fi

  cd GUI_imple/PlayerTwo
  # Establish pipes on PlayerTwo
  mkfifo pipe_send
  mkfifo pipe_recv
  # run GUI and push it in the background
  java PlayerTwoGUI &
  # PID of GUI so we can kill it when game is terminated
  GUI=$!
  # run GAME
  echo "Running Client.. connecting to server..."
  ./client $ip
  exitstatus=$?

  cd ../../PrePost_Game
  if [[ $exitstatus = 2 ]]; then
    java postGame 1
  elif [[ $exitstatus = 1 ]]; then
    java postGame 0
  else
    java postGame -1
  fi

  kill $GUI

  # Destroy pipes on PlayerTwo
  cd ../GUI_imple/PlayerTwo
  rm pipe_send
  rm pipe_recv
  cd ../../
fi

echo "GLAG Stopping..."
