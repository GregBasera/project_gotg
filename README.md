# PROJECT
---
| Name | Game of the Local Area Generals |
|------|-------|
| Description | A Client/Server implementation of the board game **GAME OF THE GENERALS** using C sockets. Given to the authors as a project in their Operating System subject course to measure the extent of their understanding of the concepts discussed. |
| Authors | Basera, Greg Emerson *gbasera@gbox.adnu.edu.ph* |
|  | Sia, Justin |  

# ENVIRONMENT
---
| Technology | Uses | Check your installation |
|------|-------|-------|
| C | Back-end processing, Socket Networking | $ gcc --version |
| Java | GUI | $ javac --version |
| FIFO | Bridging Java and C | $ mkfifo --version |
| Bash | Package Manager | $ bash --version |

# LETS PLAY!
---
1 Open terminal and clone this repository.
```
$ git clone https://GregBasera@bitbucket.org/GregBasera/project_gotg.git GLAG
```
2 Go to the directory created and compile all source files. **Make sure that all items in ENVIRONMENT are properly installed in your machine**
```
$ cd GLAG
$ ./compileAll.sh
```
3 Start the Game.
```
$ ./START_HERE_LAUNCH_ME.sh
```
4 Indicate if you are the server or the client.

5 Set the layout of your army. **The SERVER must finish this task before the CLIENT**

6 Enjoy :)
